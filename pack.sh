rm win-release.zip
jredir=$(cd $(dirname $0); pwd)"/jdk-11.0.10+9-jre"
if [ -d $jredir ]; then
echo "exiting $jredir"
else
  wget "https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.10%2B9/OpenJDK11U-jre_x64_windows_hotspot_11.0.10_9.zip"
  unzip "OpenJDK11U-jre_x64_windows_hotspot_11.0.10_9.zip"
  rm "OpenJDK11U-jre_x64_windows_hotspot_11.0.10_9.zip"
echo "downloaded $jredir"
fi
mv $jredir build/launch4j/
zip -r win-release.zip ./build/launch4j/*
